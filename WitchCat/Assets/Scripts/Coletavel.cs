﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coletavel : MonoBehaviour {
	public Animator animator;
	CircleCollider2D colisor;
	// Use this for initialization
	void Start () {
		animator = gameObject.GetComponent<Animator>();
		colisor = gameObject.GetComponent<CircleCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collider2D other){
		if(other.CompareTag("Player")){
			colisor.enabled = false;
			animator.SetTrigger("Coletando");
			Destroy(gameObject,1.375f);
		}
	}
}
