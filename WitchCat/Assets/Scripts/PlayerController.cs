
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    //floats
    public float speed;
    [SerializeField]
    private float tempoAntesDestruicao = 3.0f;
    public float jumpForce;
    public float intervaloAtaque;
    private float proximoAtaque;
    public float direcao;
    public static int vidaDoPlayer = 3;
    public float menosUmCoracao = 2;

    [SerializeField]
    private float initMana;


    //booleans
    public static bool noChao;
    private bool facingRight = true;
    private bool jump = false;


    //other


    [SerializeField] private float raio =0.3f;
    Animator animator;
    [SerializeField] private Transform groundCheck;
    [SerializeField] public LayerMask mascaraChao;
    private Rigidbody2D rb;
    Collider2D[] colliders;
    private IEnumerator coroutine;
    public bool narrativeLock = false;
    public static PlayerController Instance;

    private Stat mana;
    


    MagiaAgua waterMagic;


    void Awake() {
        Instance = this;
    }
    void Start() {
        rb = gameObject.GetComponent<Rigidbody2D>();
        direcao = Input.GetAxisRaw("Horizontal");
        groundCheck = gameObject.transform.Find("GroundCheck");
        animator = gameObject.GetComponent<Animator>();
        MagiaAgua.Instance.Poder();
        waterMagic = GetComponent<MagiaAgua>();

        mana.Initialize(initMana,initMana);
        


    }

    // Update is called once per frame
    void Update()
    {




        if (!narrativeLock)
        {

            noChao = Physics2D.OverlapCircle(groundCheck.position, raio, mascaraChao);
            animator.SetBool("Grounded", noChao);

            if (Input.GetButtonDown("Horizontal"))
            {
                rb.velocity = new Vector2(speed * direcao, rb.velocity.y);
                Debug.Log("velocidade" + rb.velocity);
            }

            if (Input.GetButtonDown("Jump") && noChao)
            {
                Debug.Log("pulo apertado");
                Jump();
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                StartCoroutine(Atacando());
            }




        }
    }
    void FixedUpdate() {
        if (!narrativeLock)
        {
            float direcao = Input.GetAxisRaw("Horizontal");
            animator.SetFloat("Walking", Mathf.Abs(direcao));
            //mantem a velocidade do personagem constante
            rb.velocity = new Vector2(direcao * speed, rb.velocity.y);

            if (direcao > 0 && !facingRight)
            {
                Flip();
            }
            else if (direcao < 0 && facingRight)
            {
                Flip();
            }
            if (jump)
            {
                rb.AddForce(new Vector2(0, jumpForce));
                jump = false;
                animator.SetBool("Grounded", false);
            }
        }

    }

    public Transform GroundCheck
    {
        get
        {
            return groundCheck;
        }
    }

    
	void Flip(){
		facingRight = !facingRight;
		//inverte o valor da coordenada x no vetor
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	IEnumerator Atacando(){
		animator.SetTrigger("Ataque");
		proximoAtaque = Time.time + intervaloAtaque;
		colliders = new Collider2D[3];
		Debug.Log("colliders" + colliders);
		transform.Find("AreaArranhao").gameObject.GetComponent<Collider2D>()
			.OverlapCollider(new ContactFilter2D(), colliders);
		for(int i = 0; i < colliders.Length; i++){
			Debug.Log("col de i" + colliders[i]);
			if(colliders[i]!=null && colliders[i].gameObject.CompareTag("Enemy")){
				yield return new WaitForSeconds(tempoAntesDestruicao);
					Destroy(colliders[i].gameObject);
			
			}
		}
	}

    IEnumerator OnCollisionEnter2D(Collision2D other)
    {
     //   if(other.gameObject.tag == "Ground")
     //   {
      //      noChao = true;
      //      animator.SetBool("Grounded", true) ;
     //   }
		if(other.gameObject.tag == "FimMapa"){
			Destroy(gameObject);
		}

		if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Fogo" && !Input.GetKey(KeyCode.X) && gameObject.tag != "PoderTerra") {
		if(vidaDoPlayer != 0.0f){
			vidaDoPlayer = vidaDoPlayer - 1;
			animator.SetTrigger("Dano");
			

		}else{
			animator.SetTrigger("Morreu");
			Debug.Log(vidaDoPlayer);
			yield return new WaitForSeconds(0.9f);
			Destroy(gameObject);
			
		}
		//other.gameObject.transform.Translate(-Vector2.right);
	}
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            noChao = false;
            Debug.Log("nao ta no chao" + noChao);
            animator.SetBool("Grounded", false);
        }
    }
    void Jump(){
		rb.AddForce(new Vector2(0,jumpForce));
        animator.SetBool("Grounded", true);
        jump = false;
	}

     

    public void NarrativeON()
    {
        narrativeLock = true;        
    
    }

    public void NarrativeOFF()
    {
        narrativeLock = false;
    }

    public void ActiveWater()
    {
        waterMagic.enabled = !waterMagic.enabled;
    }




}
