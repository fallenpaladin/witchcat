﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagiaFogo : MonoBehaviour {
	public static MagiaFogo Instance;
	Animator animator;
	Collider2D[] colisor;
	Rigidbody2D rb;

	// Use this for initialization
	void Awake(){
		Instance = this;
	}
	void Start () {
		animator = gameObject.GetComponent<Animator>();
		rb = gameObject.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.F)){
			StartCoroutine(PoderFogo());
		}
	}

	public IEnumerator PoderFogo(){
		if(Input.GetKeyDown(KeyCode.F)){
			animator.SetTrigger("Fogo");
			colisor = new Collider2D[3];
			transform.Find("PoderFogo").gameObject.GetComponent<Collider2D>()
			 .OverlapCollider(new ContactFilter2D(), colisor);
			for(int i = 0; i < colisor.Length; i++){
				Debug.Log("col de i" + colisor[i]);
				if(colisor[i]!=null && colisor[i].gameObject.CompareTag("Enemy")){
					yield return new WaitForSeconds(0.3f);
					Destroy(colisor[i].gameObject);
				}
			}
		}
	}
}
