﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagiaTerra : MonoBehaviour {
	public Animator animator;
	public Collider2D colisor;
	// Use this for initialization
	void Start () {
		animator = gameObject.GetComponent<Animator>();
		colisor = gameObject.GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void Update () {
		colisor.enabled = true;
		if(Input.GetKeyDown(KeyCode.M)){
			PoderTerra();
		}
	}

	public void PoderTerra(){
		animator.SetTrigger("Terra");
		colisor.enabled = true;		
	}
}
