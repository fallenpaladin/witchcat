﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {
	public float intervaloDeAtaque;
	private float proximoAtaque;
	Animator animator;	
	// Use this for initialization
	void Start () {
		animator = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Fire1") && Time.time > proximoAtaque){
			Atacando();
		}
	}
	void Atacando(){
			animator.SetTrigger("Ataque");
			proximoAtaque = Time.time + intervaloDeAtaque;
	}
	
}
